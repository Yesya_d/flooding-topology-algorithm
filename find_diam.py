from heapq import heappush, heappop


def dijkstra(graph, start):
    distances = {vertex: float('infinity') for vertex in graph}
    distances[start] = 0
    priority_queue = [(0, start)]

    while priority_queue:
        current_distance, current_vertex = heappop(priority_queue)

        if current_distance > distances[current_vertex]:
            continue

        for neighbor, weight in graph[current_vertex]:
            distance = current_distance + weight
            if distance < distances[neighbor]:
                distances[neighbor] = distance
                heappush(priority_queue, (distance, neighbor))

    return distances


def diameter(vertices):
    graph = {}
    for u, v, w in vertices:
        if u not in graph:
            graph[u] = []
        if v not in graph:
            graph[v] = []
        graph[u].append((v, w))
        graph[v].append((u, w))

    max_diameter = float('-inf')
    max_degree = 0
    for vertex in graph:
        distances = dijkstra(graph, vertex)
        if max_degree < len(graph[vertex]):
            max_degree = len(graph[vertex])
        max_distance = max(distances.values())
        max_diameter = max(max_diameter, max_distance)

    return max_diameter, max_degree




