import math
import random

class SLTopo():
    def __init__(self, n):
        # Number of swtiches per layer
        self.n_spines = math.ceil(n * 1 / 9)
        self.n_leafs = n - self.n_spines
        self.edges = []
        self.nodes = n

        for i in range(self.n_spines):
            for j in range(self.n_spines, self.n_leafs + self.n_spines):
                self.edges.append([i, j, 1])
        random.shuffle(self.edges)

    def print_edges(self):
        print(len(self.edges))
        print(self.nodes)
