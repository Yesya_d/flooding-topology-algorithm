from min_v_connect import *
from find_diam import *
import time
from fattree import FTTopo


def has_duplicates(list_of_lists):
    list_of_tuples = [tuple(sublist) for sublist in list_of_lists]
    set_of_tuples = set(list_of_tuples)
    return len(list_of_tuples) != len(set_of_tuples)


delta = 4
for delta in range(3, 7):
    print("DELTA = ", delta)
    for vertices_k in range(10, 91, 10):
        topos = FTTopo(vertices_k)
        print("Количество вершин:", topos.nodes)
        print("Количество ребер в исходном графе:", len(topos.edges))
        #x, y = diameter(topos.edges)
        #print("Диаметр исходного графа:", x)
        #print("Макс степень в исходном графе:", y)
        FatGraph = nx.Graph()
        #FatGraph.add_nodes_from(range(0, topos.nodes))
        #FatGraph.add_weighted_edges_from(topos.edges.astype(int))
        #nx.draw(FatGraph, pos, edge_color='red', with_labels=True)
        #plt.show()
        g = Graph(topos.nodes, len(topos.edges), FatGraph, topos.edges)
        res_graph = nx.Graph()
        start = time.time()
        res_edges = g.build_flooding(delta, single_weight=True, two_conn=True)
        #print("bridges: ", g.find_bridges(res_edges, add_to_res=False))
        finish = time.time()
        print("execution time", finish-start)
        #print(res_edges)
        res_graph.add_nodes_from(range(topos.nodes))
        res_graph.add_weighted_edges_from(res_edges)
        labels = nx.get_edge_attributes(res_graph, 'weight')

        print("Количество ребер в полученном графе:", len(res_edges))
        x, y = diameter(res_edges)
        print("Диаметр полученного графа:", x)
        print("Макс степень в полученном графе: ", y)
        print("Есть дубли?")
        if has_duplicates(res_edges):
            print("Есть")
        else:
            print("Нет")
        if len(g.find_articulation_points(res_edges)) == 0:
            print("результирующий подграф двусвязен")
        else:
            print("результирующий подграф не двусвязный")
            print("точки сочленения: ", g.find_articulation_points(res_edges))
        print("----------------------------------")
    print("###########################")