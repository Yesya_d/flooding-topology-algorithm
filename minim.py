import networkx as nx
import math
import matplotlib.pyplot as plt
import random
import numpy as np
import sys
from collections import defaultdict
from find_diam import *

sys.setrecursionlimit(10000)


class Graph:
    def __init__(self, num_vertex, num_edges, g=nx.Graph(), e=[]):
        self.num_edges = num_edges
        self.num_vertex = num_vertex
        self.edges = e
        self.graph = g
        self.node_degree = [0] * num_vertex
        self.parent = [i for i in range(self.num_vertex)]
        self.rank = [0] * num_vertex
        self.result = []
        self.edges_in_spanning_tree = 0
        self.i = 0
        self.weight_to_parent = [0] * num_vertex
        self.additional_connect = defaultdict(dict)
        self.dist_to_root = [math.inf] * num_vertex

    def add_edge(self, u, v, w):
        self.edges.append([u, v, w])

    def search(self, i):
        if self.parent[i] == i:
            return i
        return self.search(self.parent[i])

    def find_dist(self, x, visited=set(), refind=False, refind_recursive=False):
        if x in visited and self.weight_to_parent[x] != 0:
            return math.inf
        visited.add(x)
        if self.dist_to_root[x] != math.inf and not refind:
            return self.dist_to_root[x]
        dist = 0
        min = math.inf
        if len(self.additional_connect[x]) != 0:
            for i in self.additional_connect[x]:
                fd = self.find_dist(i, visited, refind_recursive)
                if fd < min:
                    min = fd
            dist = min + self.additional_connect[x][i]
            if dist != math.inf:
                d = self.weight_to_parent[x] + self.find_dist(self.parent[x], visited, refind_recursive)
                if dist > d:
                    dist = d
                if dist < self.dist_to_root[x]:
                    self.dist_to_root[x] = dist
                    return dist
                return self.dist_to_root[x]
        if self.weight_to_parent[x] != 0:
            dist = self.weight_to_parent[x] + self.find_dist(self.parent[x], visited, refind_recursive)
            self.dist_to_root[x] = dist
            return dist
        else:
            self.dist_to_root[x] = 0
            return 0

    def change_direction(self, a):         # a->..->root to root->..->a
        if self.parent[a] != a:
            self.change_direction(self.parent[a])
            self.weight_to_parent[self.parent[a]] = self.weight_to_parent[a]
            self.parent[self.parent[a]] = a

    def apply_union(self, x, y, u, v, w):      # u --> v
        xroot = self.search(x)
        yroot = self.search(y)
        if self.rank[xroot] > self.rank[yroot]:
            xroot, yroot = yroot, xroot
            u, v = v, u
        if u == xroot and v == yroot:
            self.weight_to_parent[xroot] = w
        elif u != xroot and v == yroot:
            self.change_direction(u)
            self.rank[u] = self.rank[xroot]
            xroot = u
            self.weight_to_parent[xroot] = w
        elif v != yroot:
            self.change_direction(v)
            self.rank[v] = self.rank[yroot]
            yroot = v
            self.weight_to_parent[v] = 0
            self.parent[v] = v
            if u != xroot:
                self.change_direction(u)
                self.rank[u] = self.rank[xroot]
                xroot = u
            self.weight_to_parent[xroot] = w

        self.parent[xroot] = yroot
        if self.rank[xroot] == self.rank[yroot]:
            self.rank[xroot] += 1

    def kruskal(self, delta, indexies=[]):
        iter = 0
        H = []
        if len(indexies) == 0:
            indexies = range(0, self.num_edges)

        while self.edges_in_spanning_tree < self.num_vertex - 1 and iter < len(indexies):
            u, v, w = self.edges[indexies[iter]]
            if w != 0:
                x = self.search(u)
                y = self.search(v)
                if x != y:
                    if self.node_degree[u] < delta and self.node_degree[v] < delta:
                        self.edges[indexies[iter]][2] = 0
                        self.edges_in_spanning_tree += 1
                        self.node_degree[u] += 1
                        self.node_degree[v] += 1
                        self.result.append([u, v, w])
                        self.apply_union(x, y, u, v, w)
                    else:
                        H.append(indexies[iter])
            iter += 1
        return H

    def build_flooding(self, delta,  pos=None, single_weight=False, two_conn=False, show=False):
        if not two_conn:
            self.result, points, points_edges = self.find_bridges(self.edges)
            print("Мосты найдены")

        if not single_weight:
            self.edges = sorted(self.edges, key=lambda item: item[2])

        H = self.kruskal(delta - 1)
        self.kruskal(delta, H)
        print("Получили остовное дерево?")
        x = 0
        while len(set([self.search(i) for i in range(self.num_vertex)])) != 1:
            if delta+x > self.num_vertex:
                print("Изначальный граф несвязный")
                return 0
            x += delta
            print("нет, строим с delta = ", delta + x)
            self.kruskal(delta + x)
            print("Получили остовное дерево?")
        print("да, для delta = ", delta + x)
        print("Алгоритм Краскала завершен")
        if show:
            self.print_edges(self.result, pos, num=2)

        self.diam_minimize(delta)
        print("Минимизация завершена")

        return self.result

    def print_edges(self, edges, pos, num=1):
        print("Количество вершин:", self.num_vertex)
        print("Количество ребер в графе:", len(edges))
        x, y = diameter(edges)
        print("Диаметр графа:", x)
        print("Максимальная степень в графе:", y)
        print("------------------------------------")
        labels = nx.get_edge_attributes(self.graph, 'weight')
        nx.draw_networkx_edge_labels(self.graph, pos, edge_labels=labels)
        nx.draw(self.graph, pos, with_labels=True)
        res_graph = nx.Graph()
        res_graph.add_nodes_from(range(self.num_vertex))
        res_graph.add_weighted_edges_from(edges)
        labels = nx.get_edge_attributes(res_graph, 'weight')
        nx.draw(res_graph, pos, edge_color='red', with_labels=True)
        nx.draw_networkx_edge_labels(res_graph, pos, edge_labels=labels)
        #plt.savefig(f"graph{num}.png")
        plt.show()

    def generate_random_graph(self, num_nodes, num_edges):
        self.graph.add_nodes_from(range(num_nodes))
        self.edges = np.empty(shape=[0, 3])
        for _ in range(num_edges):
            a = random.randint(0, num_nodes - 1)
            b = random.randint(0, num_nodes - 1)
            while a == b or ((len(self.edges) >= 1) and any(
                    [all([a, b] == self.edges[i, 0:2]) or all([b, a] == self.edges[i, 0:2]) for i in
                     range(len(self.edges))])):
                b = random.randint(0, num_nodes - 1)
                a = random.randint(0, num_nodes - 1)
            self.edges = np.append(self.edges, [(a, b, random.randint(1, 10))], axis=0).astype(int)
        self.graph.add_weighted_edges_from(self.edges)
        pos = nx.spring_layout(self.graph)
        labels = nx.get_edge_attributes(self.graph, 'weight')
        nx.draw_networkx_edge_labels(self.graph, pos, edge_labels=labels)
        nx.draw(self.graph, pos, with_labels=True)
        #plt.savefig("graph.png")
        return pos

    def diam_minimize(self, delta):
        for i in range(self.num_edges):
            if self.edges[i][2] != 0 and self.node_degree[self.edges[i][0]] < delta and self.node_degree[self.edges[i][1]] < delta:
                x = self.find_dist(self.edges[i][0], set(), refind=True, refind_recursive=True)
                y = self.find_dist(self.edges[i][1], set(), refind=True, refind_recursive=True)
                self.additional_connect[self.edges[i][0]][self.edges[i][1]] = self.edges[i][2]
                self.additional_connect[self.edges[i][1]][self.edges[i][0]] = self.edges[i][2]
                if x <= self.find_dist(self.edges[i][0], set(), refind=True) and y <= self.find_dist(self.edges[i][1], set(), refind=True):
                    self.additional_connect[self.edges[i][0]].pop(self.edges[i][1])
                    self.additional_connect[self.edges[i][1]].pop(self.edges[i][0])
                else:
                    self.node_degree[self.edges[i][0]] += 1
                    self.node_degree[self.edges[i][1]] += 1
                    self.result.append([self.edges[i][0], self.edges[i][1], self.edges[i][2]])
                    self.edges[i][2] = 0
        return self.result

    def find_bridges(self, edges, add_to_res=True):
        def dfs(node, parent, visited, bridges, low, disc, time):
            visited[node] = True
            disc[node] = time
            low[node] = time
            time += 1
            child_count = 0
            is_articulation = False
            for neighbor, weight, i in graph[node]:
                if not visited[neighbor]:
                    children[neighbor] = 0  # Initialize children count for the neighbor
                    child_count += 1
                    dfs(neighbor, node, visited, bridges, low, disc, time)
                    low[node] = min(low[node], low[neighbor])

                    if parent is not None and low[neighbor] >= disc[node]:
                        is_articulation = True

                    if low[neighbor] > disc[node]:
                        bridges.append((node, neighbor, weight))
                        if add_to_res:
                            self.edges[i][2] = 0
                            self.edges_in_spanning_tree += 1
                            self.node_degree[node] += 1
                            self.node_degree[neighbor] += 1
                            self.apply_union(node, neighbor, node, neighbor, weight)

                    children[node] += children[neighbor] + 1
                elif neighbor != parent:
                    low[node] = min(low[node], disc[neighbor])

            if parent is None and child_count > 1:
                articulation_points.add(node)
            if parent is not None and is_articulation:
                articulation_points.add(node)

        graph = defaultdict(list)
        for i in range(len(edges)): #edge in self.edges:
            graph[edges[i][0]].append((edges[i][1], edges[i][2], i))
            graph[edges[i][1]].append((edges[i][0], edges[i][2], i))
        bridges = []
        visited = defaultdict(bool)
        disc = defaultdict(int)
        low = defaultdict(int)
        children = defaultdict(int)
        articulation_points = set()
        time = 0

        for node in graph:
            if not visited[node]:
                dfs(node, None, visited, bridges, low, disc, time)
        points_edges = []
        for point in articulation_points:
            if point not in [bridges[i][0] for i in range(len(bridges))]+[bridges[i][1] for i in range(len(bridges))]:
                for p in graph[point]:
                    points_edges.append((point, p[0], p[1]))
        del visited
        del disc
        del low
        del graph
        return bridges, articulation_points, points_edges

    def print_data(self):
        print("parents: ", self.parent)
        print("weights to parents", self.weight_to_parent)
        print("distances to root", [self.find_dist(i, set(), refind=True) for i in range(self.num_vertex)])