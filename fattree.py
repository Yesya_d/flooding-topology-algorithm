import random


class FTTopo():
    def __init__(self, a):
        # Number of swtiches per layer
        k = a
        pod = k
        L1 = (pod // 2) ** 2
        L2 = pod * pod // 2
        L3 = L2

        self.edges = []
        self.nodes = L3 + L1 + L2

            # Creating links between switches
        # The core layer and aggregation layer links
        for i in range(L1):
            c_sw = i
            start = i % (pod // 2)
            for j in range(pod+pod%2):
                x = random.randint(0, 100)
                self.edges.append([c_sw, start + j * (pod // 2) + L1, 1])

        # aggregation and edge layer links
        for i in range(L2):
            group = i // (pod // 2)
            for j in range(pod // 2):
                x = random.randint(0, 100)
                self.edges.append([L1 + i, group * (pod // 2) + j + L1 + L2, 1])

        random.shuffle(self.edges)
        # Creating hosts and adding links between switches and hosts
        """
        for i in range(L3):
            for j in range(3):
                hs = i * 3 + j + (L3 + L2 + L1)
                self.edges.append([L1 + L2 + i, hs, 1]) """

    def print_edges(self):
        print(len(self.edges))
        print(self.nodes)


#topos = MyTopo(5)
#topos.print_edges()