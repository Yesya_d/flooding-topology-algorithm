from min_v_connect import *
from find_diam import *
import time
from spine_leaf import SLTopo


def has_duplicates(list_of_lists):
    list_of_tuples = [tuple(sublist) for sublist in list_of_lists]
    set_of_tuples = set(list_of_tuples)
    return len(list_of_tuples) != len(set_of_tuples)


for delta in range(9, 13):
    print("DELTA = ", delta)
    for vertices in range(1000, 10001, 1000):
        topos = SLTopo(vertices)
        print("Количество вершин:", topos.nodes)
        print("Количество ребер в исходном графе:", len(topos.edges))
        #x, y = diameter(topos.edges)
        #print("Диаметр исходного графа:", x)
        #print("Макс степень в исходном графе:", y)

        FatGraph = nx.Graph()
        #FatGraph.add_nodes_from(range(0, topos.nodes))
        #FatGraph.add_weighted_edges_from(topos.edges.astype(int))
        #pos = nx.kamada_kawai_layout(FatGraph)
        #nx.draw(FatGraph, pos, edge_color='red', with_labels=True)
        #plt.show()
        g = Graph(topos.nodes, len(topos.edges), FatGraph, topos.edges)
        #print(g.graph)
        #res_graph = nx.Graph()
        start = time.time()
        res_edges = g.build_flooding(delta, single_weight=True, two_conn=True)

        finish = time.time()
        print("execution time", finish-start)
    #print(res_edges)
    #res_graph.add_nodes_from(range(topos.nodes))
    #res_graph.add_weighted_edges_from(res_edges)
    #labels = nx.get_edge_attributes(res_graph, 'weight')

        print("Количество ребер в полученном графе:", len(res_edges))
        x, y = diameter(res_edges)
        print("Диаметр полученного графа:", x)
        print("Макс степень в полученном графе: ", y)
        print(max(g.node_degree))
        print("Есть дубли?")
        if has_duplicates(res_edges):
            print("Есть")
        else:
            print("Нет")

        if len(g.find_articulation_points(res_edges)) == 0:
            print("результирующий подграф двусвязен")
        else:
            print("результирующий подграф не двусвязный")
            print("точки сочленения: ", g.find_articulation_points(res_edges))
        print("----------------------------------")
    print("#############################")
