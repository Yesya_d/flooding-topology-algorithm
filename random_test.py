from min_v_connect import *
from find_diam import *
num_nodes = 10
num_edges = 20
delta = 3
g = Graph(num_nodes, num_edges)
pos = g.generate_random_graph(num_nodes, num_edges)
print("Количество вершин:", num_nodes)
print("Количество ребер в исходном графе:", len(g.edges))
x, y = diameter(g.edges)
print("Диаметр исходного графа:", x)
print("Максимальная степень в исходном графе:", y)
res_edges = g.build_flooding(delta, pos=pos, show=True)

if res_edges:
    labels = nx.get_edge_attributes(g.graph, 'weight')
    nx.draw_networkx_edge_labels(g.graph, pos, edge_labels=labels)
    nx.draw(g.graph, pos, with_labels=True)

    res_graph = nx.Graph()
    res_graph.add_nodes_from(range(num_nodes))
    res_graph.add_weighted_edges_from(res_edges)
    labels = nx.get_edge_attributes(res_graph, 'weight')
    nx.draw(res_graph, pos, edge_color='red', with_labels=True)
    nx.draw_networkx_edge_labels(res_graph, pos, edge_labels=labels)
    """g.print_data()
    print(g.additional_connect)
    print(g.common_connect)
    print("Is result graph connected?")
    if len(set([g.search(i) for i in range(num_nodes)])) == 1:
        print("yes")
        print(set([g.search(i) for i in range(num_nodes)]))
    else:
        print(set([g.search(i) for i in range(num_nodes)]))"""
    print("Количество ребер в полученной заполняющей топологии:", len(res_edges))
    x, y = diameter(res_edges)
    print("Диаметр полученной топологии:", x)
    print("Максимальная степень в полученной топологии: ", y)
    if len(g.find_articulation_points(res_edges)) == 0:
        print("результирующий подграф двусвязен")
    else:
        print("результирующий подграф не двусвязный")
        print("точки сочленения: ", g.find_articulation_points(res_edges))
    #plt.savefig("graph_final.png")
    plt.show()